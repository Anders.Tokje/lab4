package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  List<List<Color>> grid;
  int nRows;
  int nColumns;

  public ColorGrid(int nRows, int nColumns) {
    this.nRows = nRows;
    this.nColumns = nColumns;
    this.grid = new ArrayList<>();

    for (int i = 0; i < nRows; i++) {
      List<Color> inner = new ArrayList<>();

      for (int j = 0; j < nColumns; j++) {
        inner.add(null);
      }

      this.grid.add(inner);
    }
  }

  @Override
  public int rows() {
    return this.grid.size();
  }

  @Override
  public int cols() {
    return this.grid.get(0).size();
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<>();
    for (int i = 0; i < nRows; i++) {
      for (int j = 0; j < nColumns; j++) {
        CellPosition cellPos = new CellPosition(i, j);
        Color color = get(cellPos);
        CellColor cellColor = new CellColor(cellPos, color);
        cells.add(cellColor);
      }
    }
    return cells;
  }

  @Override
  public Color get(CellPosition pos) {
    return this.grid.get(pos.row()).get(pos.col());
  }

  @Override
  public void set(CellPosition pos, Color color) {
    this.grid.get(pos.row()).set(pos.col(), color);
  }
  // TODO: Implement this class
}
