package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel{

  IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400,300));
    this.grid = grid;
  }

  private void drawGrid(Graphics2D g) {

    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;

    g.setColor(MARGINCOLOR);
    Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
    g.fill(rectangle);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rectangle, this.grid, OUTERMARGIN);
    
    drawCells(g, this.grid, converter);
  }

  private static void drawCells(Graphics2D g, CellColorCollection cellColorCollection, CellPositionToPixelConverter converter) {
    // List<CellColor> cellList = cellColorCollection.getCells();
    
    for (CellColor cell : cellColorCollection.getCells()){
      CellPosition pos = cell.cellPosition();
      Rectangle2D rectangle = converter.getBoundsForCell(pos);

      if (cell.color() == null) {
        g.setColor(Color.DARK_GRAY);
      }
      else{
        g.setColor(cell.color());
      }
      g.fill(rectangle);

      }
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
    
  }
  // TODO: Implement this class
}
