package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double gridWidth = box.getWidth();
    double gridHeight = box.getHeight();

    double nRows = gd.rows();
    double nCols = gd.cols();

    double cellWidth = (gridWidth - (this.margin*(nCols + 1))) / nCols;
    double cellHeight = (gridHeight - (this.margin*(nRows + 1))) / nRows;

    double cellX = box.getX() + this.margin + cp.col() * (cellWidth + this.margin);
    double cellY = box.getY() + this.margin + cp.row() * (cellHeight + this.margin);

    
    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);

  }
}
